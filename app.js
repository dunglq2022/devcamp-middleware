//Khai báo express
const express = require ('express');

//Khai báo port
const port = 8000;

//Tạo app
const app = express();

app.use((req, res, next) => {
    console.log(new Date)
    console.log('running middileware')

    next();
})

app.post('/', (req, res) => {
    let today = new Date;
    console.log('Post Method')
    res.status(200).json({
        "message": 'Post method'
    })
})

app.get('/', (req, res) => {
    let today = new Date;
    console.log('Get method')
    res.status(200).json({
        "message": `Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()}`
    })
})

//Listen port
app.listen(port, function() {
    console.log('Server running at http://localhost:' + port);
})